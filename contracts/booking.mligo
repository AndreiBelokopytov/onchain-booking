module ERROR_MESSAGES =
    struct
        let token_id_should_be_unique = "token_id should be unique"
        let token_is_not_registered = "token_id is not registered"
        let booking_is_closed = "booking is closed"
        let wrong_price = "wrong price"
    end

type token_id = nat

type token =
[@layout:comb]
{
    price: tez;
    up_to: timestamp;
}

type registry = (token_id, token) big_map

type bookings = ((address * token_id), nat) big_map

type open_booking_param = (token_id * token) list

type close_booking_param = token_id

type book_param = token_id

type action =
    | Open_booking of open_booking_param
    | Close_booking of close_booking_param
    | Book of book_param

type storage = {
    registry: registry;
    bookings: bookings
}

let is_token_in_registry (token_id, registry: token_id * registry): bool =
    match Big_map.find_opt token_id registry with
        | Some _ -> true
        | None -> false

let is_booking_closed (token: token): bool =
    token.up_to < Tezos.get_now ()

let get_number_of_tokens (address, token_id, bookings: address * token_id * bookings): nat =
    let number_of_tokens = Big_map.find_opt (address, token_id) bookings in
    match number_of_tokens with
        | None _ -> 0n
        | Some value -> value

let open_booking (store, tokens: storage * open_booking_param): storage =
    let update_registry (next_registry, (token_id, token): registry * (token_id * token)): registry  =
        let () = assert_with_error (
            not (is_token_in_registry (token_id, next_registry))
        ) ERROR_MESSAGES.token_id_should_be_unique in
        Big_map.update token_id (Some(token)) next_registry in
    { store with registry = List.fold update_registry tokens store.registry }

let close_booking (store, token_id: storage * token_id): storage =
    let token = Big_map.find_opt token_id store.registry in
    (match token with
        | None _ -> failwith ERROR_MESSAGES.token_is_not_registered
        | Some _token ->
            let () = assert_with_error (
                not (is_booking_closed _token)
            ) ERROR_MESSAGES.booking_is_closed in
            let next_token = { _token with up_to = Tezos.get_now () - 1 } in
            let next_registry = Big_map.update token_id (Some(next_token)) store.registry in
            { store with registry = next_registry })

let book (store, token_id: storage * token_id): storage =
    let token = Big_map.find_opt token_id store.registry in
    let account = Tezos.get_sender () in
    (match token with
        | None _ -> failwith ERROR_MESSAGES.token_is_not_registered
        | Some _token ->
            let () = assert_with_error (
                Tezos.get_amount () = _token.price
            ) ERROR_MESSAGES.wrong_price in
            let () = assert_with_error (
                not (is_booking_closed _token)
            ) ERROR_MESSAGES.booking_is_closed in
            let number_of_tokens = get_number_of_tokens (account, token_id, store.bookings) in
            let next_bookings = (if number_of_tokens = 0n then
                Big_map.add (account, token_id) 1n store.bookings else
                Big_map.update (account, token_id) (Some(number_of_tokens + 1n)) store.bookings) in
            { store with bookings = next_bookings })

let main (action, store: action * storage): operation list * storage =
    ([]: operation list),
    (match action with
        | Open_booking (tokens) -> open_booking (store, tokens)
        | Close_booking (token_id) -> close_booking (store, token_id)
        | Book (token_id) -> book (store, token_id)
    )
