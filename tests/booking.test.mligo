#import "../contracts/booking.mligo" "Booking"

module TEST_DATA =
  struct
    let token : Booking.token =
    {
      price = 1000000mutez;
      up_to = ("2023-01-12t10:10:10Z" : timestamp)
    }

    let token_id = 1n
  end

let empty_storage : Booking.storage =
{
  registry = Big_map.empty;
  bookings = Big_map.empty
}

let populated_storage : Booking.storage =
{
  registry = Big_map.literal [(TEST_DATA.token_id, TEST_DATA.token)];
  bookings = Big_map.empty
}

let account = Test.nth_bootstrap_account 1

let () = Test.set_source account

let test_initial_storage =
  let (taddr, _, _) = Test.originate Booking.main empty_storage 0tez in
  assert (Test.get_storage taddr = empty_storage)

let test_open_booking =
  let (taddr, _, _) = Test.originate Booking.main empty_storage 0tez in
  let contr = Test.to_contract taddr in
  let seats = [
    (TEST_DATA.token_id, TEST_DATA.token)
  ] in
  let _ = Test.transfer_to_contract contr (Open_booking(seats)) 0mutez in
  let store = Test.get_storage taddr in
  Test.assert (Booking.is_token_in_registry (TEST_DATA.token_id, store.registry) = true)

let test_open_booking_fail_if_token_id_is_not_unique =
  let (taddr, _, _) = Test.originate Booking.main populated_storage 0tez in
  let contr = Test.to_contract taddr in
  let seats = [
    (TEST_DATA.token_id, TEST_DATA.token)
  ] in
  let result = Test.transfer_to_contract contr (Open_booking(seats)) 0mutez in
  match result with
    | Fail _ -> Test.assert true
    | Success _ -> Test.failwith "Contract must check if token_id is unique"

let test_book =
  let (taddr, _, _) = Test.originate Booking.main populated_storage 0tez in
  let contr = Test.to_contract taddr in
  let _ = Test.transfer_to_contract contr (Book(TEST_DATA.token_id)) 1000000mutez in
  let store = Test.get_storage taddr in
  Test.assert (Booking.get_number_of_tokens (account, TEST_DATA.token_id, store.bookings) = 1n)

let test_book_fail_if_token_id_is_not_registered =
  let (taddr, _, _) = Test.originate Booking.main empty_storage 0tez in
  let contr = Test.to_contract taddr in
  let result = Test.transfer_to_contract contr (Book(TEST_DATA.token_id)) 1000000mutez in
  match result with
    | Fail _ -> Test.assert true
    | Success _ -> Test.failwith "Contract must check if token_id registered"

let test_book_fail_if_wrong_price =
  let (taddr, _, _) = Test.originate Booking.main populated_storage 0tez in
  let contr = Test.to_contract taddr in
  let result = Test.transfer_to_contract contr (Book(TEST_DATA.token_id)) 0mutez in
  match result with
    | Fail _ -> Test.assert true
    | Success _ -> Test.failwith "Contract must check if sender payed correct price"

let test_book_fail_if_booking_is_closed =
  let (taddr, _, _) = Test.originate Booking.main populated_storage 0tez in
  let contr = Test.to_contract taddr in
  let _ = Test.transfer_to_contract contr (Close_booking(TEST_DATA.token_id)) 0mutez in
  let result = Test.transfer_to_contract contr (Book(TEST_DATA.token_id)) 1000000mutez in
  match result with
    | Fail _ -> Test.assert true
    | Success _ -> Test.failwith "Contract must check if booking is closed"

let test_close_booking =
  let (taddr, _, _) = Test.originate Booking.main populated_storage 0tez in
  let contr = Test.to_contract taddr in
  let _ = Test.transfer_to_contract contr (Close_booking(TEST_DATA.token_id)) 0mutez in
  let store = Test.get_storage taddr in
  match Big_map.find_opt TEST_DATA.token_id store.registry with
    | Some token -> Test.assert (Booking.is_booking_closed token)
    | None -> Test.failwith "Token does not exist"

let test_close_booking_fail_if_booking_is_closed =
  let (taddr, _, _) = Test.originate Booking.main populated_storage 0tez in
  let contr = Test.to_contract taddr in
  let _ = Test.transfer_to_contract contr (Close_booking(TEST_DATA.token_id)) 0mutez in
  let result = Test.transfer_to_contract contr (Close_booking(TEST_DATA.token_id)) 0mutez in
  match result with
    | Fail _ -> Test.assert true
    | Success _ -> Test.failwith "Contract must check if booking is closed"